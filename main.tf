##----------------------------
#     set provider
##----------------------------

provider "pass" {
  store_dir = "~/.password-store"
  refresh_store = false
}

provider "fortios" {
    alias = "fw1"
    version = "~> 2.0"
    hostname =  "${var.ipaddr_fw1}:${var.https_port_fw1}"
    token = data.pass_password.fw1.password
    insecure = true
}

provider "fortios" {
    alias = "fw2"
    version = "~> 2.0"
    hostname = "${var.ipaddr_fw2}:${var.https_port_fw2}"
    token = data.pass_password.fw2.password
    insecure = true
}


##----------------------------
#     Configure terraform
##----------------------------

terraform {
  backend "s3" {
    encrypt = true
    bucket  = "terraform-remote-state-storage-spw"
    region  = "eu-central-1"
    key     = "terraform-mod_zf_cloud_vpn_user"
  }
}

##----------------------------
#     get api token for TransitVPC firewalls
##----------------------------

data "pass_password" "fw1" {
      path = var.apitoken_path_fw1
}

data "pass_password" "fw2" {
      path = var.apitoken_path_fw2
}

##----------------------------
#  Including Provider and pass it to the module
##----------------------------

resource "fortios_user_local" "hv11266" {
  provider =  fortios.fw2
  name     =  "hv11266"
  usertype =  "radius"
  email    =  "Hartwig.Hanusch@devk.de"
  radius   =  "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11269" {
  provider =  fortios.fw2
  name     =  "hv11269"
  usertype =  "radius"
  email    =  "Axel.Gerten@devk.de"
  radius   =  "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11622" {
  provider =  fortios.fw2
  usertype =  "radius"
  name     =  "hv11622"
  email    =  "Olaf.Boehme@devk.de"
  radius   =  "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11626" {
  provider =  fortios.fw2
  name     = "hv11626"
  usertype = "radius"
  email    = "Marcel.Killich@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11627" {
  provider =  fortios.fw2
  name     = "hv11627"
  usertype = "radius"
  email    = "Ingo.Mueller@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11249" {
  provider =  fortios.fw2
  name     = "hv11249"
  usertype = "radius"
  email    = "Peter.Boxberg@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11280" {
  provider =  fortios.fw2
  name     = "hv11280"
  usertype = "radius"
  email    = "Volker.Gaal@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11650" {
  provider =  fortios.fw2
  name     = "hv11650"
  usertype = "radius"
  email    = "Ulrich.Weber@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11231" {
  provider =  fortios.fw2
  name     = "hv11231"
  usertype = "radius"
  email    = "Robert.Lau@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11667" {
  provider =  fortios.fw2
  name     = "hv11667"
  usertype = "radius"
  email    = "tobias.rademacher@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11248" {
  provider =  fortios.fw2
  name     = "hv11248"
  usertype = "radius"
  email    = "Tobias.Goecke@devk.de"
  radius    = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11577" {
  provider =  fortios.fw2
  name     = "hv11577"
  usertype = "radius"
  email    = "Tim.Stolarski@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11240" {
  provider =  fortios.fw2
  name     = "hv11240"
  usertype = "radius"
  email    = "BlasiusLofi.Dewanto@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11z04" {
  provider =  fortios.fw2
  name     = "hv11z04"
  usertype = "radius"
  email    = "Benjamin.Stein.ext@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv202x1" {
  provider =  fortios.fw2
  name     = "hv202x1"
  usertype = "radius"
  email    = "Mark.Pochert.ext@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11298" {
  provider =  fortios.fw2
  name     = "hv11298"
  usertype = "radius"
  email    = "Jan.Weitz@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11225" {
  provider =  fortios.fw2
  name     = "hv11225"
  usertype = "radius"
  email    = "Daniel.Minninger@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11671" {
  provider =  fortios.fw2
  name     = "hv11671"
  usertype = "radius"
  email    = "Maximilian.Boenig@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11595" {
  provider =  fortios.fw2
  name     = "hv11595"
  usertype = "radius"
  email    = "Kevin.Schmitz@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11x22" {
  provider =  fortios.fw2
  name     = "hv11x22"
  usertype = "radius"
  email    = "david.richter.ext@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11614" {
  provider =  fortios.fw2
  name     = "hv11614"
  usertype = "radius"
  email    = "Frank.Kewitz@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11678" {
  provider =  fortios.fw2
  name     = "hv11678"
  usertype = "radius"
  email    = "Christian.Chiout@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11x80" {
  provider =  fortios.fw2
  name     = "hv11x80"
  usertype = "radius"
  email    = "Ludwig.Mayer.ext@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11y66" {
  provider =  fortios.fw2
  name     = "hv11y66"
  usertype = "radius"
  email    = "Boris.Wetzel.ext@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11297" {
  provider =  fortios.fw2
  name     = "hv11297"
  usertype = "radius"
  email    = "Dmitry.Gurdzhi@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11608" {
  provider =  fortios.fw2
  name     = "hv11608"
  usertype = "radius"
  email    = "martin.binz@mib-network.org"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11243" {
  provider =  fortios.fw2
  name     = "hv11243"
  usertype = "radius"
  email    = "Achim.Hanke@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11739" {
  provider =  fortios.fw2
  name     = "hv11739"
  usertype = "radius"
  email    = "Frank.Mueller@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11h35" {
  provider =  fortios.fw2
  name     = "hv11h35"
  usertype = "radius"
  email    = "andreas.fritz.ext@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11258" {
  provider =  fortios.fw2
  name     = "hv11258"
  usertype = "radius"
  email    = "Katharina.Kaesmacher@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11r16" {
  provider =  fortios.fw2
  name     = "hv11r16"
  usertype = "radius"
  email    = "besir.zayim.ext@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11265" {
  provider =  fortios.fw2
  name     = "hv11265"
  usertype = "radius"
  email    = "Stefan.Winarzki@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11294" {
  provider =  fortios.fw2
  name     = "hv11294"
  usertype = "radius"
  email    = "Resul.Gaygisiz@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11228" {
  provider =  fortios.fw2
  name     = "hv11228"
  usertype = "radius"
  email    = "Ralf.Grieshaber@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11212" {
  provider =  fortios.fw2
  name     = "hv11212"
  usertype = "radius"
  email    = "Marco.Bocchini@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11375" {
  provider =  fortios.fw2
  name     = "hv11375"
  usertype = "radius"
  email = "Martin.Kaiser@devk.de"
  radius = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11x08" {
  provider =  fortios.fw2
  name     = "hv11x08"
  usertype = "radius"
  email    = "Benjamin.Boksa.ext@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11y98" {
  provider =  fortios.fw2
  name     = "hv11y98"
  usertype = "radius"
  email    = "Ulrich.Schulte.ext@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

resource "fortios_user_local" "hv11111" {
  provider =  fortios.fw2
  name     = "hv11111"
  usertype = "radius"
  email    = "hv11111@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}

/*
resource "fortios_user_local" "hv99999" {
  provider =  fortios.fw2
  name     = "hv99999"
  usertype = "radius"
  email    = "hv99999@devk.de"
  radius   = "p-rz-rr-arsa001.hv.devk.de"
}




resource "fortios_user_group" "group-terraform" {
  provider      =  fortios.fw2
  name          = "group-terraform"
  grouptype     = "firewall"
  member        = [ fortios_user_local.hv99999.name ,
                    fortios_user_local.hv11111.name
                  ]
  depends_on    = [ fortios_user_local.hv99999 ,
                    fortios_user_local.hv11111
                  ]
}*/


