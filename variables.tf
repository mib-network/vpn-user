# ---------------------------------------------------------------------------------------------------------------------
# PARAMETERS FOR 
# These parameters have reasonable defaults.
# ---------------------------------------------------------------------------------------------------------------------

variable "aws_region" {
  description = "The AWS region to deploy into"
  default     = "eu-central-1"
}

variable "aws_account" {
  default = "spielwiese"
}

variable "vpc_name" {
  description = "The AWS VPC Name"
  default     = "VPC-Name-Prod-01"
}

variable "vpc_cidr" {
  description = "The subnet cidr of your vpce"
  default     = "10.180.0.0/20"
}

# ---------------------------------------------------------------------------------------------------------------------
# PARAMETERS FOR TRANSIT-VPC (don't change default settings)
# These parameters have reasonable defaults.
# ---------------------------------------------------------------------------------------------------------------------

variable "ipaddr_fw1" {
  description = "Primary transitVPC firewall configuration interface ip"
  default = "10.175.1.10"
}

variable "ipaddr_fw2" {
  description = "Secondary transitVPC firewall configuration interface ip"
  default = "10.175.2.10"
}

variable "https_port_fw1" {
  description = "Primary transitVPC firewall configuration interface port"
  default = "10443"
}

variable "https_port_fw2" {
  description = "Secondary transitVPC firewall configuration interface port"
  default = "10443"
}

variable "apitoken_path_fw1" {
  description = "password-store path for primary firewall api-token"
  default = "firewalls/prod/tftoken-fw1"
}

variable "apitoken_path_fw2" {
  description = "password-store path for primary firewall api-token"
  default = "firewalls/prod/tftoken-fw2"
}
